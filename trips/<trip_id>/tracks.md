
# Push location data in batch to DB

```
curl --request POST \
  --url http://api.logizee.com/api/trips/<TRIP_ID>/tracks \
  --header 'Authorization: Bearer <TOKEN>' \
  --header 'Content-Type: application/json' \
  --data '{
	"batch": [
		{
			"lat": -7.288491722557636,
			"long": 112.79725646435523,
			"location": "-",
			"speed": 0,
			"distance": 0,
			"total_distance": 0,
			"created_at": "2022-09-24T06:35:22.651Z"
		}
	]
}'
```

Response

```
"success"
```

If driver has arrived, the response will be

```
"no_data"
```
