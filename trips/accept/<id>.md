# Accept driver assignment


```
curl --request PUT \
  --url http://api.logizee.com/api/trips/accept/<TRIP_ID> \
  --header 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjNmNjcyNDYxOTk4YjJiMzMyYWQ4MTY0ZTFiM2JlN2VkYTY4NDZiMzciLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiTWVkbyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9sb2dpemVlLTM1ODZmIiwiYXVkIjoibG9naXplZS0zNTg2ZiIsImF1dGhfdGltZSI6MTY2NTY3Mjg4NSwidXNlcl9pZCI6Ik5teE5VN0FXVVlmQ1NzTTZtbE1kdE9uUnN4bDIiLCJzdWIiOiJObXhOVTdBV1VZZkNTc002bWxNZHRPblJzeGwyIiwiaWF0IjoxNjY2NTEyODQ3LCJleHAiOjE2NjY1MTY0NDcsImVtYWlsIjoic3RnQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwicGhvbmVfbnVtYmVyIjoiKzYyODU3MzAwOTUwMCIsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsicGhvbmUiOlsiKzYyODU3MzAwOTUwMCJdLCJlbWFpbCI6WyJzdGdAZ21haWwuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.JT8rsQ69RvEG3tYm8-mqFWwyE-GG9sF3WwmIosHd7Y2rd4Blp7HeIl4Nxf1-Mj9goGh5D6vgKMrO7G6BqQPA9zwL8hbhL_EjXtc99Ro_qVvbAaVGrb3dS1u5lACnc0Cv7J2sAdHFgn0qLvrkob9bU1Cqn_-Fbrm3gpZLa2vjavLY_xjxvHJmdYKHkwZtwm4sodX2nAFh0ec9m1ygA4t0aBMYtT2DiGYL2lR09kdVdjdEZEekhdIvExQGLFoMN8q8qB7YBwb4eeWPDgXbwCogOk-qCXvwoSGKg4rd6v-or1BwllUGnI632FdXqlFibajQ6DTpIvCSUBs9kg4NCOOgCA'
```

### Response

```
{
	"id": 2,
	"company_id": 1,
	"fleed_id": 1,
	"driver_id": 3,
	"name": null,
	"cargo": "makanan",
	"distance": null,
	"origin": "jombang",
	"origin_lat": null,
	"origin_long": null,
	"destination": "surabaya",
	"destination_lat": null,
	"destination_long": null,
	"created_at": "2022-08-02T07:45:21.000000Z",
	"updated_at": "2022-10-23T08:19:59.000000Z",
	"depart_time": "2022-08-02 21:45:00",
	"arrival_time": "2022-08-02 21:45:00",
	"status": "in_progress",
	"is_paused": false,
	"actual_arrival": null
}
```
