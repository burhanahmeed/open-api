
# Get Current User Informations

```
curl --request GET \
  --url https://api.logizee.com/api/users/me \
  --header 'Authorization: Bearer <TOKEN>'
```

```
{
	"id": 1,
	"name": "Medo",
	"email": "dev@gmail.com",
	"phone": "+6285730095478",
	"email_verified_at": null,
	"method": null,
	"birth_place": null,
	"birth_date": null,
	"address": null,
	"no_ktp": null,
	"no_sim": null,
	"sim_expired_at": null,
	"image_path": null,
	"ktp_image_path": null,
	"sim_image_path": null,
	"ktp_verified_at": null,
	"sim_verified_at": null,
	"is_active": true,
	"created_at": "2022-06-25T02:32:57.000000Z",
	"updated_at": "2022-06-25T02:32:57.000000Z",
	"current_address": null,
	"uid": "rQ7zW4Ua23MyPApzAZvfkJxO2yv2",
	"sim_reminder_1_at": null,
	"sim_reminder_2_at": null,
	"city": null,
	"no_npwp": null,
	"npwp_path": null,
	"member": [
		{
			"user_id": 1,
			"company_id": 1,
			"role_id": 1,
			"created_at": "2022-06-25T02:32:57.000000Z",
			"updated_at": "2022-06-25T02:32:57.000000Z",
			"role": {
				"id": 1,
				"name": "transporter_owner",
				"code": "owner",
				"created_at": null,
				"updated_at": null
			}
		}
	],
	"driver_trip": {
		"id": 14,
		"driver_id": 1,
		"current_trip_id": 18,
		"created_at": null,
		"updated_at": null,
		"trip": {
			"id": 18,
			"company_id": 1,
			"fleed_id": 13,
			"driver_id": 8,
			"name": null,
			"cargo": "Hehhe and I have been",
			"distance": "3992.57",
			"origin": "Graha YKP Surabaya, Jalan Medokan Asri Utara I, Medokan Ayu, Surabaya 60297, East Java, Indonesia",
			"origin_lat": "-7.32182980",
			"origin_long": "112.79185140",
			"destination": "Sepuluh Nopember Institute of Technology, Jalan Kertajaya Indah Timur VIII, Gebang Putih, Surabaya 60115, East Java, Indonesia",
			"destination_lat": "-7.28612623",
			"destination_long": "112.79569052",
			"created_at": "2022-09-25T07:25:49.000000Z",
			"updated_at": "2022-09-25T10:14:09.000000Z",
			"depart_time": "2022-09-25 14:24:00",
			"arrival_time": "2022-09-25 15:00:00",
			"status": "finished",
			"is_paused": false,
			"actual_arrival": null
		}
	}
}
```

`driver_trip` is null if the driver has no active assignment.
