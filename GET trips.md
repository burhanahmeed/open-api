
# Get trips list for particular authenticated user

```
curl --request GET \
  --url 'http://api.logizee.com/api/trips?status[]=in_progress' \
  --header 'Authorization: Bearer <TOKEN>'
```

Response

```
{
	"current_page": 1,
	"data": [],
	"first_page_url": "http:\/\/localhost:8000\/api\/trips?page=1",
	"from": null,
	"last_page": 1,
	"last_page_url": "http:\/\/localhost:8000\/api\/trips?page=1",
	"links": [
		{
			"url": null,
			"label": "&laquo; Previous",
			"active": false
		},
		{
			"url": "http:\/\/localhost:8000\/api\/trips?page=1",
			"label": "1",
			"active": true
		},
		{
			"url": null,
			"label": "Next &raquo;",
			"active": false
		}
	],
	"next_page_url": null,
	"path": "http:\/\/localhost:8000\/api\/trips",
	"per_page": 15,
	"prev_page_url": null,
	"to": null,
	"total": 0
}
```
